# encoding: utf-8
class Schema < Sequel::Model
  plugin :timestamps
  plugin :validation_helpers

  one_to_many :schema_definitions

  def validate
    super
    validates_presence [:name, :host, :port, :database, :username, :password, :git_url]
    validates_format /\A\d+\Z/, :port
    errors.add(:port, 'が範囲外ですよ') unless (0..65535).include?(port.to_i)
    validates_unique :name
  end
end
