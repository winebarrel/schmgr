class SchemaDefinition < Sequel::Model
  plugin :timestamps
  plugin :validation_helpers

  many_to_one :shema
  many_to_one :user

  def validate
    super
    validates_presence [:ddl]
  end
end
