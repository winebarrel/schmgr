class User < Sequel::Model
  plugin :timestamps
  plugin :validation_helpers

  one_to_many :schema_definitions

  SERIALIZE_PASSWD = 'cafebabe'
  EMAIL_REGEXP = /\A[\w!\#$%&'*+\/=?^_@{}\\|~-]+(\.[\w!\#$%&'*+\/=?^_{}\\|~-]+)*@([\w][\w-]*\.)+[\w][\w-]*\Z/

  def serialize
    Crypt.encrypt(self.id.to_s, SERIALIZE_PASSWD)
  end

  def self.deserialize(serialized)
    self[Crypt.decrypt(serialized, SERIALIZE_PASSWD)]
  rescue
    nil
  end

  def validate
    super
    validates_presence [:email, :passwd_hash, :passwd_salt, :admin]
    validates_format EMAIL_REGEXP, :email
    validates_unique :email
  end
end
