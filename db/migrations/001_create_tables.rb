require File.expand_path(File.join(File.dirname(__FILE__), '../../config/environment'))

Sequel.migration do
  up do
    create_table(:schema_definitions) do
      primary_key :id
      string  :ddl,       :null => false
      integer :schema_id, :null => false
      integer :user_id,   :null => false
      string  :comment
      timestamp :applied_at
      timestamp :requested_at
      timestamp :closed_at
      timestamp :created_at
      timestamp :updated_at
    end

    create_table(:schemas) do
      primary_key :id
      string  :name,     :null => false, :unique => true
      string  :host,     :null => false
      integer :port,     :null => false, :unsigned => true
      string  :database, :null => false
      string  :username, :null => false
      string  :password, :null => false
      string  :encoding
      string  :git_url,  :null => false
      string  :git_view
      timestamp :created_at
      timestamp :updated_at
    end

    create_table(:users) do
      primary_key :id
      string  :email,       :null => false, :unique => true
      string  :passwd_hash, :null => false
      string  :passwd_salt, :null => false
      boolean :admin,       :null => false
      timestamp :created_at
      timestamp :updated_at
    end
  end

  down do
    drop_table?(:schema_definitions)
    drop_table?(:schemas)
    drop_table?(:users)
  end
end
