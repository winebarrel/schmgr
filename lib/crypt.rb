require 'openssl'
require 'bcrypt'

module Crypt
  class << self
    def generate_salt
      BCrypt::Engine.generate_salt
    end

    def hash_secret(passwd, salt)
      BCrypt::Engine.hash_secret(passwd, salt)
    end

    def encrypt(src, passwd)
      enc = OpenSSL::Cipher::BF.new
      enc.encrypt
      enc.pkcs5_keyivgen(passwd)
      (enc.update(src) + enc.final).unpack('H*').first
    end

    def decrypt(src, passwd)
      dec = OpenSSL::Cipher::BF.new
      dec.decrypt
      dec.pkcs5_keyivgen(passwd)
      dec.update([src].pack('H*')) + dec.final
    rescue OpenSSL::Cipher::CipherError
      nil
    end
  end
end
