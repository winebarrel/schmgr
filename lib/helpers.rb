class Schmgr < Sinatra::Base
  helpers do
    include Rack::Utils
    alias_method :h, :escape_html

    def current_user
      if @current_user
        @current_user
      elsif session[:current_user]
        @current_user = User.deserialize(session[:current_user])
      else
        nil
      end
    end

    def login?
      not current_user.nil?
    end

    def login_as(user)
      session[:current_user] = user.serialize
    end

    def logout!
      session.delete(:current_user)
    end

    def render_notify(bind)
      eval("erb :'/_notify', :layout => false, :locals => {:notify => defined?(notify) ? notify : nil}", bind)
    end

    def render_pagination(path, items)
      erb :'/_pagination', :locals => {:path => path, :items => items}
    end

    def strip(str)
      (str || '').strip
    end

    def blank?(str)
      strip(str).empty?
    end

    def h!(str)
      escape_html(escape_javascript(str))
    end

    def escape_javascript(html_content)
      return '' unless html_content
      javascript_mapping = { '\\' => '\\\\', '</' => '<\/', "\r\n" => '\n', "\n" => '\n' }
      javascript_mapping.merge("\r" => '\n', '"' => '\\"', "'" => "\\'")
      html_content.gsub(/(\\|<\/|\r\n|[\n\r"'])/) { javascript_mapping[$1] } # "
    end
  end
end
