require 'rubygems'
require 'bundler'; Bundler.require

require 'sinatra/base'
require 'sinatra/reloader'
require 'yaml'
require 'logger'

SINATRA_ROOT = File.expand_path(File.dirname(__FILE__) + '/..')
def SINATRA_ROOT.join(path); File.expand_path(File.join(self, path)); end

CONFIG = YAML.load_file(SINATRA_ROOT.join('config/config.yml'))

# load models
Sequel::Model.raise_on_typecast_failure = false
Sequel.extension :pagination
DB = Sequel.sqlite(SINATRA_ROOT.join(CONFIG['database']))
Dir.glob(SINATRA_ROOT.join('models/*.rb')) {|f| require f }

# load libraries
Dir.glob(SINATRA_ROOT.join('lib/*.rb')) {|f| require f }

# application configuration
class Schmgr < Sinatra::Base
  set :root, SINATRA_ROOT

  configure :development do
    register Sinatra::Reloader
    DB.logger = Logger.new($stderr)
    DB.sql_log_level = :debug
  end

  configure :production do
    logger = Logger.new(SINATRA_ROOT.join('log/production.log'))
    def logger.write(msg); self << msg; end
    use Rack::CommonLogger, logger
  end

  configure do
    enable :logging
    enable :sessions
  end
end
