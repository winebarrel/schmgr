class Schmgr < Sinatra::Base
  get '/schema' do
    erb :'/schema/index'
  end

  get '/schema/new' do
    erb :'/schema/new'
  end

  post '/schema/new' do
    errmsgs = []

    schema = Schema.new({
      :name     => params[:name],
      :host     => params[:host],
      :port     => params[:port],
      :database => params[:database],
      :username => params[:username],
      :password => params[:password],
      :encoding => params[:encoding],
      :git_url  => params[:git_url],
      :git_view => params[:git_view],
    })

    if schema.valid?
      schema.save
    else
      errmsgs.concat(schema.errors.full_messages)
    end

    if errmsgs.empty?
      redirect '/schema'
    else
      erb :'/schema/new', :locals => {:schema => schema, :notify => {:type => :error, :messages => errmsgs}}
    end
  end

  get '/schema/edit/:id' do |schema_id|
    erb :'/schema/edit', :locals => {
      :schema => Schema[schema_id],
    }
  end

  post '/schema/edit/:id' do |schema_id|
    errmsgs = []

    schema = Schema[schema_id]
    schema.name     = params[:name]
    schema.host     = params[:host]
    schema.port     = params[:port]
    schema.database = params[:database]
    schema.username = params[:username]
    schema.password = params[:password]
    schema.encoding = params[:encoding]
    schema.git_url  = params[:git_url]
    schema.git_view = params[:git_view]

    if schema.valid?
      schema.save
    else
      errmsgs.concat(schema.errors.full_messages)
    end

    if errmsgs.empty?
      redirect '/schema'
    else
      erb :'/schema/edit', :locals => {:schema => schema, :notify => {:type => :error, :messages => errmsgs}}
    end
  end

  get '/schema/destroy/:id' do |schema_id|
    schema = Schema[schema_id]
    schema.schema_definitions.each {|i| i.delete }
    schema.delete
    redirect '/schema'
  end

  get '/schema/test/:id' do |schema_id|
    schema = Schema[schema_id]

    conn_opts = {
      :host     => schema.host,
      :username => schema.username,
      :password => schema.password,
      :port     => schema.port,
      :database => schema.database,
    }

    conn_opts[:encoding] = schema.encoding if schema.encoding

    begin
      client = Mysql2::Client.new(conn_opts)
      client.query('SELECT 1')
    rescue Mysql2::Error => e
      message = e.message
    else
      message = 'Connected successfully'
    end

    content_type 'text/javascript'
    erb :'/schema/test', :layout => false, :locals => {:schema => schema, :message => message}
  end
end
