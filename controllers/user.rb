# encoding: utf-8
class Schmgr < Sinatra::Base
  before do
    unless ['/login', '/signup'].include?(request.path_info)
      redirect "/login?redirect_to=#{URI.encode(request.path_info)}" unless login?
    end
  end

  get '/login' do
    path_to_redirect = params[:redirect_to]
    path_to_redirect = '/' if blank?(path_to_redirect)
    erb :'/user/login', :locals => {:path_to_redirect => path_to_redirect}
  end

  post '/login' do
    errmsgs = []
    user = User.where(:email => params[:email]).first
    path_to_redirect = params[:redirect_to]

    if user.nil?
      errmsgs << 'ユーザが見つかりませんでした'
    elsif user.passwd_hash != Crypt.hash_secret(params[:password], user.passwd_salt)
      errmsgs << 'ユーザ名かパスワードが間違ってます'
    end

    if user and errmsgs.empty?
      login_as(user)
      redirect path_to_redirect
    else
      erb :"/user/login", :locals => {
        :path_to_redirect => path_to_redirect,
        :notify => {:type => :error, :messages => errmsgs}
      }
    end
  end

  get '/logout' do
    logout!
    redirect '/login'
  end

  get '/signup' do
    erb :'/user/signup', :locals => {:user => {}}
  end

  post '/signup' do
    errmsgs = []
    passwd = strip(params[:password])
    has_admin_priv = !strip(params[:admin]).empty?

    if /[[:graph:]]{8,}/ !~ passwd
      errmsgs << "パスワードは半角英数記号八文字以上で入力してください"
    elsif passwd != params[:password_for_confirmation]
      errmsgs << "パスワードが一致しません"
    else
      passwd_salt = Crypt.generate_salt
      passwd_hash = Crypt.hash_secret(passwd, passwd_salt)

      user = User.new({
        :email       => params[:email],
        :passwd_hash => passwd_hash,
        :passwd_salt => passwd_salt,
        :admin       => has_admin_priv,
      })

      if user.valid?
        user.save
      else
        errmsgs.concat(user.errors.full_messages)
      end
    end 

    if errmsgs.empty?
      login_as(user)
      redirect '/'
    else
      erb :'/user/signup', :locals => {
        :user => {:email => params[:email], :admin => has_admin_priv},
        :notify => {:type => :error, :messages => errmsgs}
      }
    end
  end

  get '/profile' do
    path_to_redirect = params[:redirect_to]
    erb :'/user/profile', :locals => {:path_to_redirect => path_to_redirect}
  end

  post '/profile' do
    errmsgs = []
    passwd = strip(params[:password])
    path_to_redirect = params[:redirect_to]

    user = current_user
    user.email = params[:email]
    user.admin = !strip(params[:admin]).empty?

    unless passwd.empty?
      if /[[:graph:]]{8,}/ !~ passwd
        errmsgs << "パスワードは半角英数記号八文字以上で入力してください"
      elsif passwd != params[:password_for_confirmation]
        errmsgs << "パスワードが一致しません"
      else
        user.passwd_salt = Crypt.generate_salt
        user.passwd_hash = Crypt.hash_secret(passwd, user.passwd_salt)
      end
    end

    if user.valid?
      user.save
    else
      errmsgs.concat(user.errors.full_messages)
    end

    if errmsgs.empty?
      redirect path_to_redirect
    else
      erb :'/user/profile', :locals => {
        :path_to_redirect => path_to_redirect,
        :notify => {:type => :error, :messages => errmsgs}
      }
    end
  end # post '/profile'

end # Schmgr
