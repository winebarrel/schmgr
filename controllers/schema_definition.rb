class Schmgr < Sinatra::Base
  get '/schema_definition/new' do
    schema_id = params[:schema_id]
    erb :'/schema_definition/new', :locals => {:schema_id => schema_id}
  end

  post '/schema_definition/new' do
    errmsgs = []
    schema_id = params[:schema_id]

    schema_definition = SchemaDefinition.new({
      :ddl       => params[:ddl],
      :schema_id => params[:schema_id],
      :user_id   => current_user.id,
      :comment   => params[:comment],
    })

    if schema_definition.valid?
      schema_definition.save
    else
      errmsgs.concat(schema_definition.errors.full_messages)
    end

    if errmsgs.empty?
      redirect "/#{schema_id}"
    else
      erb :'/schema_definition/new', :locals => {
        :schema_id => schema_id,
        :notify => {:type => :error, :messages => errmsgs}
      }
    end
  end # post '/schema_definition/new'

  get '/schema_definition/edit/:id' do |schema_definition_id|
    erb :'/schema_definition/edit', :locals => {
      :schema_definition => SchemaDefinition[schema_definition_id],
    }
  end

  post '/schema_definition/edit/:id' do |schema_definition_id|
    errmsgs = []
    schema_definition = SchemaDefinition[schema_definition_id]
    schema_definition.ddl = params[:ddl]
    schema_definition.comment = params[:comment]

    if schema_definition.valid?
      schema_definition.save
    else
      errmsgs.concat(schema_definition.errors.full_messages)
    end

    if errmsgs.empty?
      redirect "/#{schema_definition.schema_id}"
    else
      erb :'/schema_definition/edit', :locals => {
        :schema_definition => schema_definition,
        :notify => {:type => :error, :messages => errmsgs},
      }
    end
  end

  get '/schema_definition/destroy/:id' do |schema_definition_id|
    schema_definition = SchemaDefinition[schema_definition_id]
    schema_id = schema_definition.schema_id
    schema_definition.delete
    redirect "/#{schema_id}"
  end
end
