require SINATRA_ROOT.join('lib/helpers')

# load controllers
Dir.glob(SINATRA_ROOT.join('controllers/*.rb')) {|f| require f }

class Schmgr < Sinatra::Base
  get %r|\A/(\d*)\Z| do |schema_id|
    page_size = 10
    page = (params[:page] || 1).to_i
    page.nonzero? or page = 1

    if blank?(schema_id)
      schema = nil
      schema_definitions = []
    else
      schema = Schema[schema_id]
      schema_definitions = SchemaDefinition.where(:schema_id => schema_id).order(:id.desc).paginate(page, page_size)
    end

    erb :index, :locals => {:schema => schema, :schema_definitions => schema_definitions}
  end
end
