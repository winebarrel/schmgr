require 'rubygems'
require 'bundler'

Bundler.require

require File.expand_path(File.dirname(__FILE__) + '/config/environment')
require SINATRA_ROOT.join('controllers/app')

run Schmgr.new
